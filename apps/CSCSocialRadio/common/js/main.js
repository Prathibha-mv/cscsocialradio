/*
COPYRIGHT LICENSE: This information contains sample code provided in source code form. You may copy, modify, and distribute
these sample programs in any form without payment to IBM® for the purposes of developing, using, marketing or distributing
application programs conforming to the application programming interface for the operating platform for which the sample code is written.
Notwithstanding anything to the contrary, IBM PROVIDES THE SAMPLE SOURCE CODE ON AN "AS IS" BASIS AND IBM DISCLAIMS ALL WARRANTIES,
EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, SATISFACTORY QUALITY,
FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND ANY WARRANTY OR CONDITION OF NON-INFRINGEMENT. IBM SHALL NOT BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR OPERATION OF THE SAMPLE SOURCE CODE.
IBM HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS OR MODIFICATIONS TO THE SAMPLE SOURCE CODE.
*/

function wlCommonInit() {
	angular.element(document).ready(function() {
		angular.bootstrap(document, ['starter']);
		//location.hash = "feeds";
	});
}

/**
* Copyright 2015 IBM Corp.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
//Set the value either to "java" for a java adapter or "javascript" for a javascript adapter
var cloudantType = "javascript";
var cloudantInstance = new Cloudant(cloudantType);

var dict = [];

function wlCommonInit(){
	$("#currentType").html(cloudantType);	
	getList();
}

var list = [];
var sortedtweets=[];


function getList(){
	
	cloudantInstance.getAllEntries().then(
		function(results){			
			list = results;
		    displayList();		    
		},
		function(results){
			alert("Oops!!!" +results);
		}
	);
}


function displayList(){
	$('#list').empty();	
	
	jQuery.each(list, function(index, value){
		
		var row = $('<tr>');
		//row.append($('<td>').text(value.payload));
		row.append($('<td>').text(value.value));
		
		//row.append($('<td>').text(value.tweet.created_at));
		
		$('#list').append(row);
			
	});
	
}

$('#refresh').on('click',function(){	
	getList();
});