angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('menu.welcome', {
    url: '/home',
    views: {
      'side-menu21': {
        templateUrl: 'templates/welcome.html',
        controller: 'welcomeCtrl'
      }
    }
  })

  .state('menu.playLiveTweets', {
    url: '/live',
    views: {
      'side-menu21': {
        templateUrl: 'templates/playLiveTweets.html',
        controller: 'playLiveTweetsCtrl'
      }
    }
  })

  .state('menu.playRecentTweets', {
    url: '/recent',
    views: {
      'side-menu21': {
        templateUrl: 'templates/playRecentTweets.html',
        controller: 'playRecentTweetsCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    abstract:true
  })

  .state('cSCSocialRadio', {
    url: '/login',
    templateUrl: 'templates/cSCSocialRadio.html',
    controller: 'cSCSocialRadioCtrl'
  })

/*.state('menu.letSWatchTweets', {
    url: '/display',
    views: {
      'side-menu21': {
        templateUrl: 'templates/letSWatchTweets.html',
        controller: 'letSWatchTweetsCtrl'
      }
    }
  })*/
  
 .state('menu.playlist', {
    url: '/display',
    views: {
      'side-menu21': {
        templateUrl: 'templates/playlist.html',
        controller: 'letSWatchTweetsCtrl'
      }
    }
  })

  .state('menu.logout', {
    url: '/logout',
    views: {
      'side-menu21': {
        templateUrl: 'templates/logout.html',
        controller: 'logoutCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/login')

  

});